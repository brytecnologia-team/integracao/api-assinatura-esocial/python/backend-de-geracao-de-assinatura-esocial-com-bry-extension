# API de Geração de Assinatura eSocial com a BRy Extension

Este é exemplo backend de integração dos serviços da API de assinatura eSocial com clientes baseados em tecnologia Python, que utilizam a BRy Extension. 

Este exemplo apresenta os passos necessários para a geração de assinatura utilizando-se chave privada armazenada em disco.
  - Passo 1: Recebimento do conteúdo do certificado digital e do documento a ser assinado.
  - Passo 2: Inicialização da assinatura e produção dos artefatos signedAttributes.
  - Passo 3: Envio dos dados para cifragem no frontend.
  - Passo 4: Recebimento dos dados cifrados no frontend.
  - Passo 5: Finalização da assinatura e obtenção do artefato assinado.

### Tech

O exemplo utiliza das bibliotecas Python abaixo:
* [Requests] - Requests is an elegant and simple HTTP library for Python, built for human beings.
* [Flask] - Flask (source code) is a Python Web framework designed to receive HTTP requests.
* [Flask-Cors] - A Flask extension for handling Cross Origin Resource Sharing (CORS), making cross-origin AJAX possible.
* [Python-3.6] - Python 3.6

**Observação**

Esta API em Python deve ser executada juntamente com o [FrontEnd](https://gitlab.com/brytecnologia-team/integracao/api-assinatura-esocial/react/geracao-de-assinatura-esocial-com-bry-extension), desenvolvido em React, para que seja feita a cifragem dos dados com a extensão para web "BRy Extension"

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e instale as dependências. Utilizamos o Python versão 3.6 e pip3 para a instalação das dependências.

Comandos:

Instalar Flask:

    -pip3 install Flask

Instalar Flask-Cors

    -pip3 install -U flask-cors

Executar programa:

    -python3 example.py

  [Requests]: <https://pypi.org/project/requests/>
  [Flask]: <https://palletsprojects.com/p/flask/>
  [Flask-Cors]: <https://flask-cors.readthedocs.io/en/latest/>
  [Python-3.6]: <https://www.python.org/>
