from flask import Flask, jsonify, request
from flask_cors import CORS
import requests
import base64

URL_BASE = 'https://hub2.hom.bry.com.br'
#URL_BASE = 'https://hub2.bry.com.br'

URL_INITIALIZATION = URL_BASE + '/api/xml-signature-service/v2/signatures/initialize'
URL_FINALIZATION = URL_BASE + '/api/xml-signature-service/v2/signatures/finalize'

app = Flask(__name__)
CORS(app)

@app.route('/initialize', methods=['POST'])
def initialize_signature():

    # Passo 1 - Recebe o conteudo do certificado digital e o documento que será assinado.

    data = request.form
    file = request.files['documento'].read()

    original_documents = []
    original_documents.append(('originalDocuments[0][content]', file))

    form_initialization = {
        'nonce': 1, 
        'signatureFormat': 'ENVELOPED',
        'hashAlgorithm': 'SHA256',
        'certificate': data['certificate'],
        'profile': 'BASIC',
        'returnType': 'BASE64',
        'canonicalizerType': 'INCLUSIVE',
        'generateSimplifiedXMLDSig': 'true',
        'includeXPathEnveloped':'false',
        'originalDocuments[0][nonce]': '1',
    }


    # Passo 2 - Inicializa a assinatura, produzindo os Atributos a serem assinados e os documentos inicializados.

    response = requests.post(URL_INITIALIZATION, data = form_initialization, headers={ 'Authorization': request.headers['Authorization'] }, files=original_documents)
    if response.status_code == 200:
        reponse_data = response.json()

        # Passo 3 - Retorna os dados a serem assinados para o frontend.

        return jsonify(reponse_data)
    
    else:
        return(response.text)

@app.route('/finalize', methods=['POST'])
def finalize_signature():

    # Passo 4 - Recebe os dados assinados do frontend.

    data = request.form
    file = request.files['documento'].read()

    original_documents = []
    original_documents.append(('finalizations[0][content]', file))

    form_finalization = {
        'nonce': 1,
        'signatureFormat': 'ENVELOPED', 
        'hashAlgorithm': 'SHA256',
        'certificate': data['certificate'], 
        'profile': 'BASIC', 
        'returnType':'BASE64',
        'canonicalizerType':'INCLUSIVE',
        'finalizations[0][nonce]': 1, 
        'finalizations[0][initializedDocument]': data['initializedDocuments'],
        'finalizations[0][signatureValue]': data['cifrado']
    }

    # Passo 5 - Finalização da assinatura e obtenção do artefato assinado.

    response = requests.post(URL_FINALIZATION, data = form_finalization, headers={ 'Authorization': request.headers['Authorization'] }, files=original_documents)
    if response.status_code == 200:
        data = response.json()
        base64_string = data[0]
        signature = base64.b64decode(base64_string.encode("utf-8"))
        signed_file = open('./files/signatureESocial.xml', 'wb')
        signed_file.write(signature)
        signed_file.close()
        return jsonify(data)

    else:
        print("Erro na finalização", response.text)
        return "{}"

if __name__ == '__main__':
    app.run(host="localhost", port=5000, debug=True)